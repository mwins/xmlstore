<link href='http://fonts.googleapis.com/css?family=Didact+Gothic' rel='stylesheet' type='text/css'>

<style>

*{font-family: 'Didact Gothic', sans-serif;}






</style>


<h1> XML Data Store Example</h1>


<?php 
/** XML Store
* Stores Key/Value pairs for use inside the script.
* @package MASTER_PRODUCT_NAME
* @subpackage xml_store
* @version 0.1.1
*
*/
include 'xs-load.php';
$file = "xs_data.xml";
$sxe = xmlstore_init();
 $k = 'locker';
 $v = null;

 
 
 if(isset($_GET['process']))
 {
 $k = $_GET['key'];
 $v = $_GET['value'];
 $sxe->store($k,$v);
$sxe->write($file);
 
 }
 
?>
<div style="width: 408px; height: 454px; padding: 1px; background: rgba(9,9,9,.05);overflow:hidden;border: rgba(9,9,9,.9) 1px solid;
position: relative;
box-sizing: border-box;
">
<div style="width: 400px;height: 450px;border: rgba(9,9,9,.3) 1px solid;background: rgba(0,0,0,.3);color: #fff;
font-size: 1.1em; text-shadow: 0px 3px 2px rgba(150, 150, 150, 1);
text-align:right;
padding: 0 2px;
margin:1px;
max-height: 100%;
max-width:100%;
box-sizing: border-box;
">
 
<?php 


 
$list = $sxe->fetch_all();
 echo '<table>' ;
 
 foreach($list as $item)
	{
		echo '<tr><td>'. $item->key . '</td><td>'. $item->value . '</td></tr>';
 
 }
echo  '</table>';
 
 

?>

<form>
<input type="hidden" name="process" value="1" />
<label>
key</label>
<input type="text" name="key" required  value="<?php echo $k;?>"/>

<label>Value </label>
<input type="text" name="value" required  />

<input type="submit" value="Store KV" />
</form>

<form>
<input type="hidden" name="process" value="1" />
 
<input type="hidden" name="key"   value="<?php echo $k;?>"/>

<label>Add Your Locker : </label>
<input type="text" name="value" required  />

<input type="submit" value="Store KV" />
</form>

</div>