<?php 
/** 
* XMLStore - simple data storage
*
*
*/

class XMLStore extends SimpleXMLElement {
  
	function store($key,$value,$ref=null)
	{
		  $date = time();
		// check if the data store exists
		$this_node = $this->store->AddChild('item');
		$this_node->AddChild('key',$key);
		$this_node->AddChild('value',$value);
		$this_node->AddChild('date',$date );
		$this_node->AddChild('ref',$ref);
	 }
 
 function write($file)
	{
		$this->asXML($file);
	
	}
  
function fetch($key,$ref ='',$format=1)
{
if(empty($ref))
	{
	foreach($this->store->item as $item)
	{
		if($item->key == $key)
		{
			if($format ==1)
				$return[] = array($item->key,$item->value,$item->date,$item->ref);	
			else
				$return[] = $item->value;
		}
	} 
	}
	else
	foreach($this->store->item as $item)
	{
		if($item->key == $key && $item->ref == $ref)
		{
			if($format ==1)
				$return[] = array($item->key,$item->value,$item->date,$item->ref);	
			else
				$return[] = $item->value;
		}
	} 	
	return $return;
}  
  
function fetch_all()
{
	foreach($this->store->item as $item)
	{
	$return[] = $item;
  
	}
	return $return;
 }
  
  
  
  
function remove($key,$ref ='')
{
$i = 0;
$rm = null;
if(empty($ref))
	{
	foreach($this->store->item as $item)
	{
		if($item->key == $key )
		{
			$rm[] = $i;
		}
		$i++;
	}
	}
else
	{
	foreach($this->store->item as $item)
	{
		if($item->key == $key && $item->ref == $ref)
		{
			$rm[] = $i;
		}
		$i++;
	}
	}
	if(is_array($rm))
	{
	// reverse array to maintain index values 
	$rm = array_reverse($rm);
	foreach($rm as $r)
		{
		unset($this->store->item[$r]);
		}
	}
}
  
function fetch_ref($ref)
	{
	foreach($this->store->item as $item)
	{
		if($item->ref == $ref)
		{
			$return[] = $item;
		}
	}
	return $return;
	}
	
	
	
	
	
  
  
  //end
}