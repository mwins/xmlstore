## XML Store 

* Language PHP
* Version 5+
* Requires  SimpleXML

### What it is 

Simple datastore using xml for persistent data storage. 

### How to use

Download. Include xmlstore.class.php in project files. Create a new object via the simplexml_load_string or simplexml_load_file functions and include 'XMLStore' as the 2nd parameter to use the extended XMLStore class.

### Usage

* xmlstore_init() - will create a xs_data.xml in the current directory. If a path/filename is provided, it will look for the file at that location. Note, it will not create directories or a file if it does not exist. Only call this once. This returns the XMLStore object to use. $sxe from here on.

* $sxe->store($key,$value,$ref) - Adds a record to the in memory XML Data Store.
* $sxe->fetch($key,$ref) - Returns records based on key or key and ref. 
* $sxe->remove($key,$ref) - Removes records based on key or key and ref.
* $sxe->write($file) - Writes the XML file to disk. 
