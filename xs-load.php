<?php 

include 'xmlstore.class.php';

function xmlstore_init($file = 'xs_data.xml')
	{
	if(file_exists($file))
	{
		$sxe = simplexml_load_file($file,'XMLStore',LIBXML_NOCDATA);
	}
	else 
	{
		$root = 'xml';
		$xmlstr = '<' . $root . ' />';
		$sxe = simplexml_load_string($xmlstr,'XMLStore',LIBXML_NOCDATA);
		$sxe->AddChild('store');	
		$sxe->asXML($file);	
	}
	
	return $sxe;
	}